$(document).ready(function(){




		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active');
					}

					else{
						$('.arrow_up').removeClass('active');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				$(".resp_btn_menu, .close_resp_menu").on("click ontouchstart", function(){
					$("body").toggleClass("show_menu")
				});

				$(document).on("click ontouchstart", function(event) {
			      if ($(event.target).closest("nav,.resp_btn_menu").length) return;
			      $("body").removeClass("show_menu");
			      if(windowW <= 991){
			      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
			      }
			      event.stopPropagation();
			    });

				// проверка на наличие элемента и вставка хтмл кода
			 	//  if($(window).width() <= 767){
				// 	if ($(".menu_item").length){
				//         $(".menu_item").has('.dropdown-menu').append('<span class="touch-button"><i class="fa fa-angle-down"></i></span>');
				//     }
				// }
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 991){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown-menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown-menu")
			             .slideUp();
			          }
			        }

			    });




		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */




		/* ------------------------------------------------
		FOCUS INPUT START
		------------------------------------------------ */

			   $(".input_field").live("focus blur", function(e){
			      var $this = $(this),
			          parentInput = $(".search_box");
			      setTimeout(function(){
			        $this.toggleClass("focused_child", $this.is(":focus"));
			        $this.parent(parentInput).toggleClass("focused_parent", $this.is(":focus"));
			      }, 0);
			    });

		/* ------------------------------------------------
		FOCUS INPUT END
		------------------------------------------------ */




		/* ------------------------------------------------
		BLOCK POSITION START
		------------------------------------------------ */

				//скрипт  считывает сколько ширина скрола начало

				var scrollWidth2;
				function detectScrollBarWidth2(){
					var div = document.createElement('div');
						cssObj   = {"position": "absolute","top": "-9999px","width": "50px","height": "50px","overflow": "scroll"};	
					div.className = "detect_scroll_width";
					$(div).css(cssObj);
					document.body.appendChild(div);
					scrollWidth2 = div.offsetWidth - div.clientWidth;
					document.body.removeChild(div);
					// console.log(scrollWidth2);
				}
				detectScrollBarWidth2();

				//скрипт  считывает сколько ширина скрола конец

				// скрипт переставляет блок на определенной ширине в другой блок начало
				
				function filterPosition(){
		          var bodyWidth = $(window).width();
		          if(bodyWidth + scrollWidth2 <= 768 && !$('body').hasClass('filterPosition')){
		            $('.resp_box_onMenu').append($('.header_top_search'));
		            $('body').addClass('filterPosition');
		          }
		          else if(bodyWidth + scrollWidth2 > 768 && $('body').hasClass('filterPosition')){
		            $("#header").find('.header_numbers').after($('.header_top_search'));
		            $('body').removeClass('filterPosition');
		          }
		        } 
		        filterPosition();
				$(window).on('resize',function(){
			        setTimeout(function(){
		            	filterPosition();
		            },200);
	            });
		            setTimeout(function(){
		            	filterPosition();
		            },200);


				// скрипт переставляет блок на определенной ширине в другой блок конец

		/* ------------------------------------------------
		BLOCK POSITION END
		------------------------------------------------ */



		
});